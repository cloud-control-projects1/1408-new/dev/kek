package com.company.kek;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@SpringBootApplication
@ConfigurationPropertiesScan
public class KekApplication {

    public static void main(String[] args) {
        SpringApplication.run(KekApplication.class, args);
    }
}
